---
name: Bug report
about: Report a bug, problem, issue or a missing feature
labels: bug

---

**Description**
A clear and concise description of the problem.

**Scope**
Where in *Git example tutorial* is this bug happening.
E.g.
- Compilation
- Core
- Documentation

**To Reproduce**
Steps to reproduce the bug (if apply). Add any custom example files here if relevant.

**Expected behavior**
A clear and concise description of what should be the expected behaviour.

**Environment**
Please specify your operating system, *Git example tutorial* branch, compiler version or any other library versions that you consider relevant.
E.g.:
- OS: Ubuntu 18.04
- Branch: master
- Compiler: GCC-6
