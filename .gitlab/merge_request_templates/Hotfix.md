---
name: Hotfix
about: Quick bugfix with very localized and low impact changes.

---

# Closes #XXXX

**Description**
A brief description of the bugfix or the improvements.
If it has an associated issue please add "Closes #XXXX".

**Changelog**
Please summarize the change in one line to generate the changelog:
E.g.
- Fixed Y (#XXXX)


**How to test**

List of steps to follow in order to reproduce this changes.
E.g.
- Compile XXXX
- Run script YYYY

**Misc**

Commentaries not related with the previous sections.
