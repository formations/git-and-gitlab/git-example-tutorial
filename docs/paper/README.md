# Paper

This is a fake paper in order to do an example for LaTeX edition using git.

## Generation

This has been generated using [mathgen](https://thatsmathematics.com/mathgen/).

## Compile

To recompile this file, run:

pdflatex paper
bibtex paper
pdflatex paper
pdflatex paper

You need the following packages installed:

AMS-LaTeX
fullpage
mathrsfs
natbib
truncate
