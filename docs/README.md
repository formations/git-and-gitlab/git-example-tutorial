# Documents

 This folder contains the documents of the tutorial. 
 
- [Presentation folder](presentation): Template original from [here](https://intranet.inria.fr/cmis/download/1253776/1).
- [Paper folder](paper): Fake paper for testing the *LaTeX* code edition example. 

# Additional documentation

Tutorials from Anthony Baire:

- [GIT for Beginners](https://people.irisa.fr/Anthony.Baire/git/git-for-beginners-handout.pdf)
- [GIT Advanced](https://people.irisa.fr/Anthony.Baire/git/git-advanced-handout.pdf)
- [GIT for SVN users](https://people.irisa.fr/Anthony.Baire/git/git-for-svn-users-handout.pdf)
