%Chapitre 2
\section{Git}
\frame{\sectionpage}

\subsection{Introduction}

\begin{frame}{Introduction}

    \begin{center}
        \begin{figure}[h]
            \includegraphics[width=0.5\textwidth]{git.png}
            \caption{Git branches}
        \end{figure}
    \end{center}

    \vspace{-0.75cm}

    \begin{block}{What is Git?}
        Git is a distributed version control system - which just means that when you do a \texttt{git clone} what you are actually getting is a complete copy of your entire history of that project.
    \end{block}

    \begin{alertblock}{What are the advantages of Git?}
        \begin{itemize}
            \item{Git has a staging area.}
            \item{Unlike centralized version control systems, Git branches are cheap and easy to merge.}
        \end{itemize}
    \end{alertblock}

\end{frame}

\begin{frame}{SVN vs Git}
    \begin{center}
        \begin{figure}[h]
            \includegraphics[width=1.0\textwidth]{svn_vs_git.png}
            \caption{Common commands \textbf{SVN} vs \textit{Git}}
        \end{figure}
    \end{center}
\end{frame}

\subsection{Basic commands}

\begin{frame}[fragile]{Basic commands (I)}

    \tiny
    \begin{block}{See what branch you're on}
        \begin{lstlisting}[language=bash]
            git status
        \end{lstlisting}
        It will return the current working branch. If a file is in the staging area, but not committed, it shows with git status. Or, if there are no changes it willl return nothing to commit, working directory clean.
    \end{block}

    \tiny
    \begin{block}{Working with local repositories}
        \begin{lstlisting}[language=bash]
            git init
        \end{lstlisting}
        This command turns a directory into an empty \textit{Git} repository.
    \end{block}

    \tiny
    \begin{block}{Adding files}
        \begin{lstlisting}[language=bash]
            git add <file or directory name>
        \end{lstlisting}
        Adds files in the to the staging area for \textit{Git}.
    \end{block}

    \tiny
    \begin{block}{Commitig changes}
        \begin{lstlisting}[language=bash]
            git commit -m `Commit message in quotes`
        \end{lstlisting}
        Record the changes made to the files to a local repository. For easy reference, each commit has a unique ID.
    \end{block}

\end{frame}

\begin{frame}[fragile]{Basic commands (II)}

    \tiny
    \begin{block}{List all branches}
        To see local branches, run this command:
        \begin{lstlisting}[language=bash]
            git branch
        \end{lstlisting}
        To see remote branches, run this command:
        \begin{lstlisting}[language=bash]
            git branch -r
        \end{lstlisting}
        To see all local and remote branches, run this command:
        \begin{lstlisting}[language=bash]
            git branch -a
        \end{lstlisting}
    \end{block}

    \tiny
    \begin{block}{Create a new branch}
        Run this command (replacing my-branch-name with whatever name you want):
        \begin{lstlisting}[language=bash]
            git checkout -b my-branch-name
        \end{lstlisting}
    \end{block}

    \tiny
    \begin{block}{Switch to a branch in your local repo}
        \begin{lstlisting}[language=bash]
            git checkout my-branch-name
        \end{lstlisting}
    \end{block}
\end{frame}

\begin{frame}[fragile]{Basic commands (III)}

    \begin{center}
        \begin{figure}[h]
            \includegraphics[width=0.5\textwidth]{cherrypicking.png}
            \caption{Cherrypicking}
        \end{figure}
    \end{center}

    \vspace{-0.75cm}

    \tiny
    \begin{block}{Cherrypicking changes}
        \begin{lstlisting}[language=bash]
            git cherry-pick <commit-hash>
        \end{lstlisting}
        Cherry picking in \textit{Git} means to choose a commit from one branch and apply it onto another.
    \end{block}

    What is a commit-hash?, the identification of each commit as follows:

    \begin{center}
        \begin{figure}[h]
            \includegraphics[width=0.5\textwidth]{commit_hash.png}
            \caption{Commit-hash}
        \end{figure}
    \end{center}

\end{frame}

\begin{frame}[fragile]{Basic commands (IV)}

    \tiny
    \begin{block}{Switch to a branch that came from a remote repo}
        To get a list of all branches from the remote, run this command:
        \begin{lstlisting}[language=bash]
            git pull
        \end{lstlisting}
        Run this command to switch to the branch:
        \begin{lstlisting}[language=bash]
            git checkout --track origin/my-branch-name
        \end{lstlisting}
        Or you can use:
        \begin{lstlisting}[language=bash]
            git branch my-branch-name origin/my-branch-name
        \end{lstlisting}
        With newer versions, you can simply use:
        \begin{lstlisting}[language=bash]
            git fetch -p
            git checkout branchxyz
        \end{lstlisting}
    \end{block}

    \tiny
    \begin{block}{Push to a branch}
        If your local branch does not exist on the remote, run either of these commands:
        \begin{lstlisting}[language=bash]
            git push -u origin my-branch-name # or
            git push -u origin HEAD
        \end{lstlisting}
        If your local branch already exists on the remote, run this command:
        \begin{lstlisting}[language=bash]
            git push
        \end{lstlisting}
    \end{block}
\end{frame}

\begin{frame}[fragile]{Basic commands (V)}

    \tiny
    \begin{block}{Merge a branch}
        Checkout to the branch you want to be merge the changes:
        \begin{lstlisting}[language=bash]
            git checkout branch_to_merge_changes
        \end{lstlisting}
        Now in order to merge the changes of interest:
        \begin{lstlisting}[language=bash]
            git merge branch_with_changes_to_be_merged
        \end{lstlisting}
    \end{block}

    \tiny
    \begin{block}{Delete branches}
        To delete a remote branch, run this command:
        \begin{lstlisting}[language=bash]
            git push origin --delete my-branch-name
        \end{lstlisting}
        To delete a local branch, run either of these commands:
        \begin{lstlisting}[language=bash]
            git branch -d my-branch-name # or
            git branch -D my-branch-name # Shortcut for --delete --force
        \end{lstlisting}
    \end{block}

    \tiny
    \begin{block}{Clone remote repository}
        Cloning is the equivalent of git init when working with a remote repository.
        \begin{lstlisting}[language=bash]
            git clone <remote_URL>
        \end{lstlisting}
    \end{block}
\end{frame}

\subsection{Advanced commands}

\begin{frame}[fragile]{Advanced commands}

    \tiny
    \begin{block}{Stashing changes}
        To save changes made when they're not in a state to commit them to a repository. This will store the work and give a clean working directory. For instance, when working on a new feature that's not complete, but an urgent bug needs attention.
        \begin{lstlisting}[language=bash]
            # Store current work with untracked files
            git stash -u

            # Bring stashed work back to the working directory
            git stash pop
        \end{lstlisting}
    \end{block}

    \tiny
    \begin{block}{Log information}
        To show the chronological commit history for a repository. This helps give context and history for a repository. git log is available immediately on a recently cloned repository to see history.
        \begin{lstlisting}[language=bash]
            # Show entire git log
            git log

            # Show git log with date pameters
            git log --<after/before/since/until>=<date>

            # Show git log based on commit author
            git log --<author>="Author Name"
        \end{lstlisting}
    \end{block}

\end{frame}

\subsection{Comparing branches}

\begin{frame}[fragile]{Comparing whole branches}

    \begin{center}
        \begin{figure}[h]
            \includegraphics[width=0.95\textwidth]{kompare_branches.png}
%             \caption{Comparison}
        \end{figure}
    \end{center}

    \vspace{-0.5cm}

    \tiny
    \begin{block}{Comparing whole branches}
        \begin{lstlisting}[language=bash]
            git config --global diff.tool NAME_OF_COMPARATION_SOFTWARE
            git branch (to see the local branches checked out in your computer)
            git difftool -d master..feature_branch
        \end{lstlisting}
    \end{block}

\end{frame}

\begin{frame}{Recommended software for comparing}

    \begin{columns}
        \column{0.45\textwidth}

        \begin{alertblock}{Meld}
            \tiny
            Meld is a visual diff and merge tool targeted at developers. Meld helps you compare files, directories, and version controlled projects. It provides two- and three-way comparison of both files and directories, and has support for many popular version control systems.
        \end{alertblock}

        \begin{center}
            \begin{figure}[h]
                \includegraphics[width=1.0\textwidth]{meld.png}
                \caption{Meld}
            \end{figure}
        \end{center}
        \column{0.45\textwidth}

        \begin{alertblock}{Kompare}
            \tiny
            Kompare is a graphical user interface program that allows you to view and merge the differences between source code files. It can be used to compare the differences between files or folder content. It allows different \texttt{diff} formats and provides many options to customize the level of information it displays.
        \end{alertblock}


        \begin{center}
            \begin{figure}[h]
                \includegraphics[width=1.0\textwidth]{kompare.png}
                \caption{Kompare}
            \end{figure}
        \end{center}
    \end{columns}

\end{frame}

\subsection{Git private repository}

\begin{frame}[fragile]{Git with private repository?}

    \tiny
    \begin{block}{Generate your SSH public key}
        Generate public key:
        \begin{lstlisting}[language=bash]
            ssh-keygen -o
        \end{lstlisting}
        Retrieve public key:
        \begin{lstlisting}[language=bash]
            cat ~/.ssh/id_rsa.pub
        \end{lstlisting}
    \end{block}

    \tiny
    \begin{block}{Adding to GitLab}
        \begin{figure}[h]
            \subfloat[Go to \textbf{SSH} settings]{\includegraphics[width=0.425\textwidth]{SSH-Key-5.jpg}}
            \subfloat[Add key]{\includegraphics[width=0.575\textwidth]{SSH-Key-2.jpg}}
            \caption{Adding \textbf{SSH} key to \textit{Gitlab}}
        \end{figure}
    \end{block}

\end{frame}

\subsection{Git clients}

\begin{frame}{Git clients}
    \begin{columns}
        \column{0.4\textwidth}
        List of recommended clients for \textit{Git}:
        \begin{center}
            \begin{figure}[h]
                \includegraphics[width=1.0\textwidth]{sourcetree.png}
                \caption{Sourcetree}
            \end{figure}
        \end{center}
        \column{0.4\textwidth}
        \begin{center}
            \begin{figure}[h]
                \includegraphics[width=1.0\textwidth]{gitkraken.png}
                \caption{GitKraken}
            \end{figure}
        \end{center}

        \vspace{-0.75cm}

        \begin{center}
            \begin{figure}[h]
                \includegraphics[width=1.0\textwidth]{git_tower.png}
                \caption{Git tower}
            \end{figure}
        \end{center}
    \end{columns}
\end{frame}

\subsection{Example git commands}

\begin{frame}[fragile]{Example git commands (I)}

    \begin{alertblock}{}
        See \texttt{git-example-tutorial/examples/git-commands/}
    \end{alertblock}

    \vspace{-0.25cm}

    \footnotesize
    \begin{block}{Steps}
        \begin{enumerate}
            \item Create git repository from scratch: In new folder \texttt{git init}
            \item Add some commits that we will use for this example: Create a document
            \begin{enumerate}
                \item \texttt{git add file}
                \item \texttt{git commit -m 'n-th commit'}
            \end{enumerate}
            \item Create new branch
            \begin{enumerate}
                \item \texttt{git checkout -b new-branch}
                \item \texttt{git branch -a}
            \end{enumerate}
            \item Cherrypicking: Do some other commits, copy the hash
        \end{enumerate}

        \vspace{-0.2cm}

        \begin{lstlisting}[language=bash]
            git commit -a -m "Remove unused"
            [new-branch da23d1b] Remove unused
            git checkout master
            git cherry-pick da23d1b
        \end{lstlisting}
    \end{block}

\end{frame}

\begin{frame}[fragile]{Example git commands (II)}

    \begin{alertblock}{NOTE}
        Add \texttt{SSH} first
    \end{alertblock}

    \footnotesize
    \begin{block}{Steps}
        \begin{enumerate}
            \item Deleting branch: \texttt{git branch -D new-branch}
            \item Create a remote repository
        \end{enumerate}
    \end{block}

    \begin{columns}
        \column{0.45\textwidth}
        \begin{center}
            \begin{figure}[h]
                \includegraphics[width=1.0\textwidth]{create_project.png}
%                 \caption{Create project}
            \end{figure}
        \end{center}
        \column{0.45\textwidth}
        \begin{center}
            \begin{figure}[h]
                \includegraphics[width=1.0\textwidth]{blank_project.png}
%                 \caption{Blank project}
            \end{figure}
        \end{center}
    \end{columns}

\end{frame}

\begin{frame}[fragile]{Example git commands (III)}

    \begin{columns}
        \column{0.45\textwidth}
        \begin{center}
            \begin{figure}[h]
                \includegraphics[width=1.0\textwidth]{test_project_1.png}
            \end{figure}
        \end{center}
        \column{0.45\textwidth}
        \begin{center}
            \begin{figure}[h]
                \includegraphics[width=1.0\textwidth]{test_project_2.png}
            \end{figure}
        \end{center}
    \end{columns}

    \footnotesize
    \begin{block}{}
        \begin{lstlisting}[language=bash]
            git remote add origin git@gitlab_url
            git add .
            git push -u origin master
        \end{lstlisting}
    \end{block}

\end{frame}

\begin{frame}[fragile]{Example git commands (IV)}

    \footnotesize
    \begin{block}{Steps}
        \begin{enumerate}
            \item Push new branch to the remote repo:
            \begin{enumerate}
                \item \texttt{git checkout -b new-branch}
                \item Do some changes: \texttt{git commit -a -m 'Changes'}
                \item Push: \texttt{git push -u origin HEAD}
            \end{enumerate}
        \end{enumerate}
    \end{block}

    \begin{center}
        \begin{figure}[h]
            \includegraphics[width=0.5\textwidth]{new_branch_origin.png}
        \end{figure}
    \end{center}
\end{frame}

\begin{frame}[fragile]{Example git commands (V)}

    \footnotesize
    \begin{block}{}
        We move back to master and remove the new branch:
        \begin{lstlisting}[language=bash]
            git checkout master
            git branch -d new_branch
        \end{lstlisting}

        Now we can retrieve the branch from the origin:
        \begin{lstlisting}[language=bash]
            git fetch
            git checkout new_branch
        \end{lstlisting}
    \end{block}

    \footnotesize
    \begin{block}{Steps}
        \begin{enumerate}
            \item Merge changes to master:
            \begin{enumerate}
                \item \texttt{git checkout master}
                \item \texttt{git merge new-branch}
            \end{enumerate}
        \end{enumerate}
    \end{block}
\end{frame}

\begin{frame}[fragile]{Example git commands (VI)}
    \footnotesize
    \begin{block}{Steps}
        \begin{enumerate}
            \item Stash:
            \begin{enumerate}
                \item Store current work: \texttt{git stash -u}
                \item Bring stashed work back: \texttt{git stash pop}
            \end{enumerate}
            \item Check log: \texttt{git log}
            \item We compare the branches
        \end{enumerate}

        \begin{lstlisting}[language=bash]
            git config --global diff.tool SOFTWARE
            git branch
            git difftool -d master..feature_branch
        \end{lstlisting}
    \end{block}


\end{frame}

% \begin{frame}{Titre}
%     Du \textcolor{gris_fonce_inria}{texte}  {en}  \textcolor{rouge_inria}{couleur}
% \end{frame}
%
% \begin{frame}{Titre}
%     \begin{exampleblock}{}
%         \begin{itemize}
%             \item{Item 1 }
%             \item {Item 2}
%             \item {Item 3}
%         \end{itemize}
%     \end{exampleblock}
%
%     \begin{exampleblock}{Avec titre}
%         \begin{itemize}
%             \item{Item 1 }
%             \item {Item 2}
%             \item {Item 3}
%         \end{itemize}
%     \end{exampleblock}
% \end{frame}
