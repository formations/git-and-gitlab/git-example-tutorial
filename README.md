Git example tutorial
=================

<div align="center">

![image](https://gitlab.inria.fr/formations/git-and-gitlab/git-example-tutorial/-/wikis/uploads/46ab0ca9582ee94ba108f0dc8903830b/image.png)

</div>

[[_TOC_]]

# Introduction

<p align="left">
    ![https://gitlab.inria.fr/formations/git-and-gitlab/git-example-tutorial/-/blob/master/LICENSE](https://img.shields.io/badge/License-Beerware-yellow.svg?style=flat-square)
    ![pipeline status](https://gitlab.inria.fr/formations/git-and-gitlab/git-example-tutorial/badges/master/pipeline.svg?style=flat-square)
</p>


This repository is an example for "playing" with git commands for the **COMMEDIA** team.

# Documents

The [documents folder](docs/) contains the sources files for the presentation.

Presentation can be found in [here](https://formations.gitlabpages.inria.fr/git-and-gitlab/git-example-tutorial/).

# Examples

Examples shown in the presentation can be found in the [examples folder](examples/).

# Wiki

You can find the wiki of this repository in [here](https://gitlab.inria.fr/formations/git-and-gitlab/git-example-tutorial/-/wikis/home).
