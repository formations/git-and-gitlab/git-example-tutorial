# Examples
 
 This folder contains examples shown in the slides.
 
 - [`git-commands`](git-commands): Tutorial of the basic *Git* commands.
 - [`github-flow`](github-flow): Tutorial of the *Gitlab* workflow.
 - [`LaTeX`](LaTeX): Example of using *Git* with *LaTeX* documents.