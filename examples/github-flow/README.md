# Github flow example

## Objetive

The example is show how the *Github* flow works in *Gitlab*. In order to do so, we will modify a test file so the **CI** will fail, this will show us a complete workflow.

## Steps

### Create a new branch for the editing

For example:

~~~sh
git checkout -b your_username_modify_test
~~~

### Go to the test folder

`git-example-tutorial/tests/`

### Edit the file so it fails

For example the file `run_tests.py`:

~~~tex
# Import unittest
import unittest

class TestStringMethods(unittest.TestCase):

    def test_upper(self):
        self.assertEqual('foo'.upper(), 'FOO')

    def test_isupper(self):
        self.assertFalse('FOO'.isupper())
        self.assertTrue('Foo'.isupper())

    def test_split(self):
        s = 'hello world'
        self.assertEqual(s.split(), ['hello', 'world'])
        # check that s.split fails when the separator is not a string
        with self.assertRaises(TypeError):
            s.split(2)

if __name__ == '__main__':
    unittest.main()
~~~

### Push the changes

First we add and commit the changes:

~~~sh
git commit -a -m "Making the test fail"
~~~

The `-a` avois the need to first add the changes, as in this case we have only modified one file and we just want to add all the changes.

Then we can push to the remote repository:

~~~sh
git push --set-upstream origin your_username_modify_test
~~~

Depending of our configuration of `git` and characteristics of the repository we will need to provide the password.

### Open a MR

We create a **MR** as in any other project:

![](create_mr_workflow.png)

![](mr_creation_workflow.png)

#### Discuss the MR

Once the **MR** has been created it is possible to discuss the changes, serving as feedback on the changes.

![](ci_fails.png)

![](comments_workflow.png)

#### Correct the errors

We open Web IDE for example, as changes are easy to do direclty from there (and because we want it to show hot to do use it).

![](open_web_ide.png)

![](edit_with_web_ide.png)

#### Now test passes

![](test_passes.png)

#### Resolve issues

![](resolve_issues.png)

![](comment_resolve_issues.png)

#### Approve and  merge

![](merge_workflow.png)
