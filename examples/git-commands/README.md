# Git commands example

## Objetive

The objective of this tutorial is control the basic *Git* commands.

## Steps

### Create git repository from scratch

First we create local repository that we will use to "play". In my case I created a folder named *test*, and inside this test:

~~~sh
git init
~~~

### Add some commits that we will use for this example

We will create a `README.md` in two stages:

First stage:

~~~markdown
#A h1 header

Paragraphs are separated by a blank line.

2nd paragraph. *Italic*, **bold**, ***both*** and `monospace`.


Here is a bullet list:

- apple
	- green apple
	- red apple
- banana
- orange

Here is a numbered list:

1. apple
	1. green apple
	2. red apple
2. banana
3. orange

> Block quotes are written like so.
> They can span multiple paragraphs, if you like.

>> You can also have a nested block quote

~~~

Then we do:

~~~sh
git add README.md
git commit -m "First commit"
~~~

Second stage:

~~~markdown

## A h2 header

To write code, you indent it by 4 spaces or a tab. Remarkable will guess what language you are writing

	def main():
		print "Hello World"


You can also use delimited blocks like so

~~~

Then we do:

~~~sh
git add README.md
git commit -m "Second commit"
~~~

Third stage:

~~~markdown
### A h3 header ###

Here is a link to the Remarkable website
 [Remarkable](http://remarkableapp.github.io)

Remarkable will also auto-detect links, e.g: google.com

Here is how to specify an image
![Alt text](https://remarkableapp.github.io/images/remarkable.png)
~~~

Then we do:

~~~sh
git add README.md
git commit -m "Third commit"
~~~

###  Create new branch

We create a new branch:

~~~sh
git checkout -b new_branch
~~~

We see the list of branches:

~~~sh
git branch -a
~~~

### Cherrypicking

Do some changes in the file (for example remove text), and then commit:

~~~sh
git commit -a -m "Remove unused"
~~~

The outpuit will be something like:

~~~sh
[new_branch da23d1b] Remove unused
 1 file changed, 1 insertion(+), 51 deletions(-)
~~~

Then change to the master again:

~~~sh
git checkout master
~~~

Now we can  cherrypick using the hash of ou previous commit:

~~~sh
git cherry-pick da23d1b
~~~

### Deleting branch

We can delete the new branch now:

~~~sh
git branch -D new_branch
~~~

### Create a remote repository

Go to your *Inria Gitlab account*, and create a *test* project.

![](create_project.png)

![](blank_project.png)

![](test_project_1.png)

![](test_project_2.png)

Then:

~~~sh
git remote add origin git@gitlab.inria.fr:username/test.git
git add .
git push -u origin master
~~~

### Push new branch to the remote repo

Create a new branch:

~~~sh
git checkout -b new_branch
~~~

Do some changes:

~~~sh
git commit -a -m "Some changes"
~~~

We push the branch:

~~~sh
git push -u origin HEAD
~~~

Now the branch is available in *Inria Gitlab*:

![](new_branch_origin.png)

We move back to *master* and remove the new branch:

~~~sh
git checkout master
git branch -d new_branch
~~~

Now we can retrieve the branch from the origin:

~~~sh
git fetch -p
git checkout new_branch
~~~

**NOTE:** The `-p` option in the `git fetch` command allows to prune the branches that does not exist anymore.

### Merge changes to master

In order to merge changes from the new branch for example:

~~~sh
git checkout master
git merge new_branch
~~~

### Stash

Do some changes and then store current work with untracked files:

~~~sh
git stash -u
~~~

Bring stashed work back to the working directory:

~~~sh
git stash pop
~~~

### Check log

Show entire git log:

~~~sh
git log
~~~

### We compare the branches

~~~sh
git config --global diff.tool NAME_OF_COMPARATION_SOFTWARE
git branch (to see the local branches checked out in your computer)
git difftool -d master..feature_branch
~~~
