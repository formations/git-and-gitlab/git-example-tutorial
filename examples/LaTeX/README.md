# LaTeX collaborative edition
 
## Objetive
 
 Show that it is possible to edit a LaTeX code in a collaborative way.
 
## Steps

### Create a new branch for the editing

For example:

~~~sh
git checkout -b your_username_edit_paper
~~~

### Go to the paper folder

`git-example-tutorial/docs/paper/`

### Edit the file as please 

For example add a new section at the end that will be discussed:

~~~tex
\section{Acknowledgements}

Thank you to my supervisor, Dr.  Ivo Robotnik Eggman, for providing guidance and feedback throughout this project. Thanks also to my wife Anna, for putting up with me being sat in the office for hours on end, and for providing guidance and a sounding board when required.
~~~

### Push the changes

First we add and commit the changes:

~~~sh
git commit -a -m "Adding Acknowledgements"
~~~

The `-a` avois the need to first add the changes, as in this case we have only modified one file and we just want to add all the changes.

Then we can push to the remote repository:

~~~sh
git push --set-upstream origin your_username_edit_paper 
~~~

Depending of our configuration of `git` and characteristics of the repository we will need to provide the password.

### Open a MR

We create a **MR** as in any other project:

![](create_mr_latex.png)

![](mr_creation_latex.png)

### Discuss the MR 

Once the **MR** has been created it is possible to discuss the changes, serving as feedback on the changes.

![](comments_latex.png)