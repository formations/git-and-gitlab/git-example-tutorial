# Docker

This folder contains the Docker files

To build docker images, the command shall be launched in the project root folder.

```
docker build -f docker/<dockerfile> .
```

The .dockerignore file is used to reduce docker image size by keeping the docker context to minimum.

